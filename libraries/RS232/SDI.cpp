/*! \file Wasp232.cpp
    \brief Library for managing RS-232 module
    
    Copyright (C) 2014 Libelium Comunicaciones Distribuidas S.L.
    http://www.libelium.com
 
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 2.1 of the License, or
    (at your option) any later version.
   
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.
  
    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
  
    Version:		1.2
    Design:			David Gascón
    Implementation:	Ahmad Saad

 */


/***********************************************************************
 * Includes
 ***********************************************************************/

#include "SDI.h"

#ifndef __WPROGRAM_H__
	#include "WaspClasses.h"
#endif 


 /***********************************************************************
 * Methods of the Class
 ***********************************************************************/
 
 
 void SDI12::wakeSensors(){
  digitalWrite(_dataPin, HIGH); 
  delayMicroseconds(12100); 
  digitalWrite(_dataPin, LOW); 
  delayMicroseconds(8400);
}

void SDI12::hold(){
	
 
//!*************************************************************
//!	Name: ON()
//!	Description: Powers the 232 module and opens the UART
//!	Param: socket: the socket to be used (0 or 1)
//!	Returns: void
//!*************************************************************
void SDI12::ON(char socket)
{
	if (socket == SOCKET1) 
	{
		// set Mux
		Utils.setMuxSocket1();
		
		// power on
		pinMode(DIGITAL6, OUTPUT);
		digitalWrite(DIGITAL6, HIGH);
		
		// update attribute
		_uart = SOCKET1;
	}
	

	if(_uart == SOCKET1)	WaspRegister |= REG_SOCKET1;
	
	// wait stabilization time
	delay(100);
	
}


//!*************************************************************
//!	Name:	OFF()
//!	Description: Switches off the module and closes the UART
//!	Param : void
//!	Returns: void
//!*************************************************************
void SDI12::OFF(void)
{
	closeSerial(_uart);
	
	if(_uart == SOCKET1)	WaspRegister &= ~(REG_SOCKET1);	

	digitalWrite(DIGITAL6, LOW);

}


//!*************************************************************
//!	Name: baudRateConfig()
//!	Description: It sets the speed of asynchronous communication
//!	Param : unsigmed log, speed to configure
//!	Returns: void
//!*************************************************************
void SDI12::baudRateConfig(unsigned long speed)
{
	_baudrate = speed;
	beginSerial(speed, _uart);
}


//!*************************************************************
//!	Name: read()
//!	Description: Receives data through the UART
//!	Param : void
//!	Returns: char, data read
//!************************************************************* 
char SDI12::read(void)
{
	
	return serialRead(_uart);
}

//!*************************************************************
//!	Name: receive()
//!	Description: Receives data through the UART
//!	Param : void
//!	Returns the number of received bytes	
//!************************************************************* 
uint16_t SDI12::receive(void)
{
	uint16_t nBytes = 0;
	nBytes = readBuffer(sizeof(_buffer));
	
	return nBytes;
}


//!*************************************************************
//!	Name: send()
//!	Description: It sends an unsigned char(uint_8) n
//!	Param : uint8_t, data to send
//!	Returns: void
//!*************************************************************
void SDI12::send(uint8_t n) 
{
	printByte(n, _uart);
	delay(_delay);
}


//!*************************************************************
//!	Name: send()
//!	Description: It sends an int n
//!	Param: int, data to send
//!	Returns: void
//!*************************************************************
void SDI12::send(int n)
{
	if (n < 0) {
		printByte('-', _uart);
		n = -n;
	}
	
	printIntegerInBase(n, 10, _uart);
	delay(_delay);
} 


//!*************************************************************
//!	Name: send()
//!	Description: It sends an unsigned int
//!	Param : unsigned int, data to send
//!	Returns: void
//!*************************************************************
void SDI12::send(unsigned int n)
{
	printIntegerInBase(n, 10, _uart);
	delay(_delay);
}


//!*************************************************************
//!	Name: send()
//!	Description:  It sends a long
//!	Param: long, data to send
//!	Returns: void
//!*************************************************************
void SDI12::send(long n)
{
	if (n < 0) {
		printByte('-', _uart);
		n = -n;
	}

	printIntegerInBase(n, 10, _uart);
	delay(_delay);
}


//!*************************************************************
//!	Name: send()
//!	Description: It sends a string
//!	Param : const char s, the string to send
//!	Returns:void
//!*************************************************************
void SDI12::send(const char *s)
{
	printString(s, _uart);
	delay(_delay);
}


//!*************************************************************
//!	Name: send()
//!	Description: It sends an unsigned long
//!	Param : unsigned long, data to send
//!	Returns: void
//!*************************************************************
void SDI12::send (unsigned long n)
{
	printIntegerInBase(n, 10, _uart);
	delay(_delay);
}


//!*************************************************************
//!	Name: send()
//!	Description: 
//!	Param: param long n, unsigned long to send.
//!			param int base, the base for printing the number
//!	Returns: void
//!*************************************************************
void SDI12::send(long n, int base)
{	
	if (base == 0)
		printByte((char) n ,_uart);
	else
		printIntegerInBase(n, base, _uart);
	
	delay(_delay);
}


//!*************************************************************
//!	Name: available()
//!	Description: Get the number of bytes available for reading
//!	Param: void
//!	Returns: the number of bytes available
//!*************************************************************
int SDI12::available(void)
{
	return serialAvailable(_uart);
}


//!*************************************************************
//!	Name: flush()
//!	Description: Flushes the buffer of incoming serial data
//!	Param: void
//!	Returns: void
//!*************************************************************
void SDI12::flush(void)
{
	serialFlush(_uart);
}


//!*************************************************************
//!	Name: parityBit()
//!	Description: Enables (EVEN or ODD) or disables the parity bit
//!	Param : bool state: "EVEN", "ODD" or "DISABLED"
//!	Returns: 						
//!*************************************************************
void SDI12::parityBit(uint8_t state)
{

	if (_uart == SOCKET1) 
	{
		if (state == DISABLED) 	
		{
			cbi(UCSR1C, UPM11);
			cbi(UCSR1C, UPM10);
		}
		if (state == EVEN)				
		{
			sbi(UCSR1C, UPM11);
			cbi(UCSR1C, UPM10);
		}
		if (state == ODD)		
		{
			sbi(UCSR1C, UPM11);
			sbi(UCSR1C, UPM10);
		}
	}
}



//!*************************************************************
//!	Name:	stopBitConfig()
//!	Description: Selects the number of stop bits to be inserted
//!	Param:  uint8_t numStopBits, the number of stop bits
//!	Returns: void
//!*************************************************************
void SDI12::stopBitConfig(uint8_t numStopBits)
{

	if (_uart == SOCKET1) 
	{
		if (numStopBits == ONE_STOP_BIT)
		{
			cbi(UCSR1C, USBS1);
		}
		if (numStopBits == TWO_STOP_BITS)
		{
			sbi(UCSR1C, USBS1);
		}
	}
}


//!*************************************************************
//!	Name: error()
//!	Description: Detects a transmission error
//!	Param: void
//!	Returns: 	error = 0 if no error in transmision
//!				error = 1	if frame error
//!				error = 2	if data over run
//!				error = 3	if parity error
//!*************************************************************
uint8_t SDI12::error(void) 
{ 
	if (_uart == 1) {
		if ((UCSR1A & 0x1C) ==0) return 0; //!No error
		if (UCSR1A & 0x10) return 1; //!Frame error
		if (UCSR1A & 0x08) return 2; //! Data over run 
		if (UCSR1A & 0x04) return 3; //! Parity error
	}
}


//**********************************************************************
// Private functions
//**********************************************************************

void SDI12::print(char c, uint8_t uart) 
{
	printByte(c, uart);
}


void SDI12::printNumber (unsigned long	n, uint8_t	base, uint8_t uart)
{			
	printIntegerInBase(n, base, uart);
}


SDI12 SDI = SDI12();
