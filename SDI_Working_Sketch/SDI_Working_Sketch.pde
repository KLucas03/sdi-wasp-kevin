
// Put your libraries here (#include ...)
#include <WaspUtils.h>

/* Storing these arduino definitions here for now
//#define digitalPinToPCICR(p)    (((p) >= 0 && (p) <= 21) ? (&PCICR) : ((uint8_t *)0))
//#define digitalPinToPCICRbit(p) (((p) <= 7) ? 2 : (((p) <= 13) ? 0 : 1))
//#define digitalPinToPCMSK(p)    (((p) <= 7) ? (&PCMSK2) : (((p) <= 13) ? (&PCMSK0) : (((p) <= 21) ? (&PCMSK1) : ((uint8_t *)0))))
//#define digitalPinToPCMSKbit(p) (((p) <= 7) ? (p) : (((p) <= 13) ? ((p) - 8) : ((p) - 14)))
*/

//1.1 - max buffer size
//#define _dataPin = MUX_RX
//#define _BUFFER_SIZE = 256;
//char _rxBuffer[_BUFFER_SIZE];  // 1.2 - buff for incoming
uint8_t _rxBufferHead = 0;     // 1.3 - index of buff head
uint8_t _rxBufferTail = 0;     // 1.4 - index of buff tail



void setup() {
    // put your setup code here, to run once:
    USB.ON();
}


void loop() {
    // put your main code here, to run repeatedly:
  PWR.setSensorPower(SENS_5V,SENS_ON);
  delay(350);
//  setMuxAux1();
  pulse();
  PWR.setSensorPower(SENS_5V,SENS_OFF);
  delay(16000);
}
void pulse() {
  USB.println("Powering ON...");
  delay(50);
  pinMode(MUX_RX,OUTPUT);
  delay(15);
  digitalWrite(MUX_RX,LOW);
  //delayMicroseconds(2);
  delay(5);
  USB.println("Sending Pulse...");//////////////////////
  digitalWrite(MUX_RX, HIGH);      // I think you may be able to use 1 instead of high and 0 instead of LOW. Check tomorrow
  delayMicroseconds(12100);       //
  digitalWrite(MUX_RX, LOW); 
  delayMicroseconds(8400);
  pinMode(MUX_RX,INPUT);

  USB.println("Changed to input..");
  delay(5);
}
/*  
}
void setState(uint8_t state){
  if(state == HOLDING){
    pinMode(_dataPin,OUTPUT);
    digitalWrite(_dataPin,LOW);
    *digitalPinToPCMSK(_dataPin) &= ~(1<<digitalPinToPCMSKbit(_dataPin));
  }
  if(state == TRANSMITTING){
    pinMode(_dataPin,OUTPUT);
    noInterrupts();
  }
  if(state == LISTENING) {
    digitalWrite(_dataPin,LOW);
    pinMode(_dataPin,INPUT); 
    interrupts();               // supplied by Arduino.h, same as sei()
    *digitalPinToPCICR(_dataPin) |= (1<<digitalPinToPCICRbit(_dataPin));
    *digitalPinToPCMSK(_dataPin) |= (1<<digitalPinToPCMSKbit(_dataPin));
    /*
    *EIMSK &= ~(1 << INT3);	
    *EICRA = (EICRA & ~((1 << ISC30) | (1 << ISC31))) | (mode << ISC30); // We are shifting both ISC30 and ISC31 to the 1 status 
    *EIFR  |= (1 << INT3);                                               // which means a RISING EDGE interrupt on MUX_RX Pin generates an interrupt. 
                                                                        // More info at: http://www.atmel.com/Images/Atmel-2549-8-bit-AVR-Microcontroller-ATmega640-1280-1281-2560-2561_datasheet.pdf page 110
                                                                        
    *EIMSK |= (1 << INT3);                                                //Could this replace the code above it? Looks very similar. 
                                                                       //Found in wiring_digital.c in API
  } else {                      // implies state==DISABLED 
    digitalWrite(_dataPin,LOW); 
    pinMode(_dataPin,INPUT);
    
    *EIMSK &= ~(1 << INT2);
    if(!*EIMSK){
      EICRA = ~(EICRA & ~((1 << ISC30) | (1 << ISC31))) | (mode << ISC30);    //Could this replace the code below??
    }
    
    *digitalPinToPCMSK(_dataPin) &= ~(1<<digitalPinToPCMSKbit(_dataPin));
    if(!*digitalPinToPCMSK(_dataPin)){
        *digitalPinToPCICR(_dataPin) &= ~(1<<digitalPinToPCICRbit(_dataPin));
    }
 }
  
// 2.2 - forces a HOLDING state. 
void forceHold(){
    setState(HOLDING); 
}

void SDI12(uint8_t dataPin){
  _bufferoverflow = false;
  _dataPin = dataPin;
}

void 

*/

