/*  
 *  ------ Waspmote Pro Code Example -------- 
 *  
 *  Explanation: This is the basic Code for Waspmote Pro
 *  
 *  Copyright (C) 2013 Libelium Comunicaciones Distribuidas S.L. 
 *  http://www.libelium.com 
 *  
 *  This program is free software: you can redistribute it and/or modify  
 *  it under the terms of the GNU General Public License as published by  
 *  the Free Software Foundation, either version 3 of the License, or  
 *  (at your option) any later version.  
 *   
 *  This program is distributed in the hope that it will be useful,  
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of  
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
 *  GNU General Public License for more details.  
 *   
 *  You should have received a copy of the GNU General Public License  
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.  
 */
     
// Put your libraries here (#include ...)
#include <WaspUtils.h>

void setup() {
    // put your setup code here, to run once:
    USB.ON();
}


void loop() {
    // put your main code here, to run repeatedly:
  PWR.setSensorPower(SENS_5V,SENS_ON);
  delay(350);
//  setMuxAux1();
  pulse();
  PWR.setSensorPower(SENS_5V,SENS_OFF);
  delay(16000);
}
void pulse() {
  USB.println("Powering ON...");
  delay(50);
  pinMode(MUX_TX,OUTPUT);
  delay(15);
  digitalWrite(MUX_TX,LOW);
  //delayMicroseconds(2);
  delay(5);
  USB.println("Sending Pulse...");
  digitalWrite(MUX_TX, HIGH); 
  delayMicroseconds(12100); 
  digitalWrite(MUX_TX, LOW); 
  delayMicroseconds(8400);
  pinMode(MUX_TX,INPUT);
  //pinMode(MUX_TX, INPUT);
  //  USB.println("Changed to input..");
  
}

