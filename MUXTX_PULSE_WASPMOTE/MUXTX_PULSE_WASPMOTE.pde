
/*
    ------ Waspmote Pro Code Example --------

    Explanation: This is the basic Code for Waspmote Pro

    Copyright (C) 2016 Libelium Comunicaciones Distribuidas S.L.
    http://www.libelium.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// Put your libraries here (#include ...)


void setup()
{
USB.ON();
RTC.ON();

}


void loop()
{
 USB.println("Welcome. Starting loop...");
 //setMuxAux1();
  PWR.setSensorPower(SENS_5V,SENS_ON);
  delay(350);
  USB.println("Powering ON...");
  delay(50);
  pinMode(MUX_TX,OUTPUT);
  digitalWrite(MUX_TX,LOW);
  USB.println("Sending Pulse...");
  digitalWrite(MUX_TX, HIGH); 
  delayMicroseconds(12100); 
  digitalWrite(MUX_TX, LOW); 
  delayMicroseconds(8400);
  pinMode(MUX_TX,INPUT);
  //attachInterrupt(3, onHAIwakeUP, RISING);
  //USB.println("Pulse sent, restarting process.");
  //PWR.setSensorPower(SENS_5V,SENS_OFF);
  delay(7000);
}
/*void 
inline void tunedDelay(uint16_t delay) { 
  uint8_t tmp=0;

  asm volatile("sbiw    %0, 0x01 \n\t"
    "ldi %1, 0xFF \n\t"
    "cpi %A0, 0xFF \n\t"
    "cpc %B0, %1 \n\t"
    "brne .-10 \n\t"
    : "+w" (delay), "+a" (tmp)
    : "0" (delay)
    );
}
void WaspUtils::setMuxAux1()
{
  pinMode(MUX_PW, OUTPUT);
  pinMode(MUX_0, OUTPUT);      
  pinMode(MUX_1, OUTPUT);   
  digitalWrite(MUX_PW, HIGH);   
  digitalWrite(MUX_0, HIGH);
  digitalWrite(MUX_1, LOW);
}
/*
float pulseIn(uint8_t pin, uint8_t state)
{
  // cache the port and bit of the pin in order to speed up the
  // pulse width measuring loop and achieve finer resolution.  calling
  // digitalRead() instead yields much coarser resolution.
  uint8_t bit = digitalPinToBitMask(pin);
  uint8_t port = digitalPinToPort(pin);
  uint8_t stateMask = (state ? bit : 0);
  unsigned long width = 0; // keep initialization out of time critical area
  
  // wait for the pulse to start
  while ((*portInputRegister(port) & bit) != stateMask)
    ;
  
  // wait for the pulse to stop
  while ((*portInputRegister(port) & bit) == stateMask)
    width++;

  // convert the reading to microseconds. The loop has been determined
  // to be 10 clock cycles long and have about 12 clocks between the edge
  // and the start of the loop. There will be some error introduced by
  // the interrupt handlers.
  float clockCyclesPerMicrosecond = (float)F_CPU / (float)1000000;  
  return (width * 10 + 12)/clockCyclesPerMicrosecond;
  
}
*/
