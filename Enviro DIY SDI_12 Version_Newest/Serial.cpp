#ifndef __WASPCONSTANTS_H__
	#include "WaspConstants.h"
#endif
#include "wiring_private.h"

/*
char _rxBuffer[_BUFFER_SIZE]; 	// 1.2 - buff for incoming
uint8_t _rxBufferHead = 0;		// 1.3 - index of buff head
uint8_t _rxBufferTail = 0;		// 1.4 - index of buff tail
*/

	unsigned char rx_buffer1[RX_BUFFER_SIZE_1];
	uint8_t rx_buffer_head1 = 0;
	uint8_t rx_buffer_tail1 = 0;
	
	
	// connects the internal peripheral in the processor and configures it
void beginSerial(long baud, uint8_t portNum)
{
	if (portNum == 0) {
		setIPF_(IPUSART0);
		UBRR0H = ((F_CPU / 16 + baud / 2) / baud - 1) >> 8;
		UBRR0L = ((F_CPU / 16 + baud / 2) / baud - 1);
		
		// enable rx and tx
		sbi(UCSR0B, RXEN0);
		sbi(UCSR0B, TXEN0);
		
		// enable interrupt on complete reception of a byte
		sbi(UCSR0B, RXCIE0);
		
		
	} else {
		setIPF_(IPUSART1);
		UBRR1H = ((F_CPU / 16 + baud / 2) / baud - 1) >> 8;
		UBRR1L = ((F_CPU / 16 + baud / 2) / baud - 1);
	
		// enable rx and tx
		sbi(UCSR1B, RXEN1);
		sbi(UCSR1B, TXEN1);
		
		// enable interrupt on complete reception of a byte
		sbi(UCSR1B, RXCIE1);
	}
	// defaults to 8-bit, no parity, 1 stop bit
}

// disconnects the internal peripheral in the processor
void closeSerial(uint8_t portNum)
{
	if (portNum == 0) {
		// turn off the internal peripheral, but also the interface
		// resetIPF is just turning off the clock, what is not helping
		// to save power, you gotta get rid of all the pull-ups in the sytem
		resetIPF_(IPUSART0);
 		cbi(UCSR0B, RXEN0);
                cbi(UCSR0B, TXEN0);
	} else {
		// turn off the internal peripheral, but also the interface
		// resetIPF is just turning off the clock, what is not helping
		// to save power, you gotta get rid of all the pull-ups in the sytem
		resetIPF_(IPUSART1);
 		cbi(UCSR1B, RXEN1);
                cbi(UCSR1B, TXEN1);
	}
}

void SDI12::writeChar(uint8_t out)
{

  out |= (parity_even_bit(out)<<7);	  // 4.2.1 - parity bit
  SDI12.parityBit(EVEN);

  digitalWrite(_dataPin, HIGH); 			// 4.2.2 - start bit
  delayMicroseconds(SPACING);

  for (byte mask = 0x01; mask; mask<<=1){	// 4.2.3 - send payload
    if(out & mask){
      digitalWrite(_dataPin, LOW);
    }
    else{
      digitalWrite(_dataPin, HIGH);
    } 
    delayMicroseconds(SPACING);
  }
  
  digitalWrite(_dataPin, LOW);				// 4.2.4 - stop bit
  delayMicroseconds(SPACING); 
}

//	4.3	- this function sends out the characters of the String cmd, one by one
void SDI12::sendCommand(String cmd){
  wakeSensors();							// wake up sensors
  for (int i = 0; i < cmd.length(); i++){
	writeChar(cmd[i]); 						// write each characters
  }	
  setState(LISTENING); 						// listen for reply
}
void serialWrite(unsigned char c, uint8_t portNum)
{
	if (portNum == 0) {
		while (!(UCSR0A & (1 << UDRE0)))
			;

		UDR0 = c;
	} else {
		while (!(UCSR1A & (1 << UDRE1)))
			;

		UDR1 = c;
	}
}

int serialAvailable(uint8_t portNum)
{
	if (portNum == 0)
		return (RX_BUFFER_SIZE_0 + rx_buffer_head0 - rx_buffer_tail0) % RX_BUFFER_SIZE_0;
	else
		return (RX_BUFFER_SIZE_1 + rx_buffer_head1 - rx_buffer_tail1) % RX_BUFFER_SIZE_1;
}

int serialRead(uint8_t portNum)
{
	if (portNum == 0) {
		// if the head isn't ahead of the tail, we don't have any characters
		if (rx_buffer_head0 == rx_buffer_tail0) {
			return -1;
		} else {
			unsigned char c = rx_buffer0[rx_buffer_tail0];
			rx_buffer_tail0 = (rx_buffer_tail0 + 1) % RX_BUFFER_SIZE_0;
			return c;
		}
	}
	else {
		// if the head isn't ahead of the tail, we don't have any characters
		if (rx_buffer_head1 == rx_buffer_tail1) {
			return -1;
		} else {
			unsigned char c = rx_buffer1[rx_buffer_tail1];
			rx_buffer_tail1 = (rx_buffer_tail1 + 1) % RX_BUFFER_SIZE_1;
			return c;
		}
	}
}

void serialFlush(uint8_t portNum)
{
	// don't reverse this or there may be problems if the RX interrupt
	// occurs after reading the value of rx_buffer_head but before writing
	// the value to rx_buffer_tail; the previous value of rx_buffer_head
	// may be written to rx_buffer_tail, making it appear as if the buffer
	// were full, not empty.
	if (portNum == 0){
		rx_buffer_tail0=0;
		rx_buffer_head0 = rx_buffer_tail0;
	}
	else{
		rx_buffer_tail1=0;
		rx_buffer_head1 = rx_buffer_tail1;
	}
}
	
void SDI::wakeSensors(){
  setState(TRANSMITTING); 
  digitalWrite(_dataPin, HIGH); 
  delayMicroseconds(12100); 
  digitalWrite(_dataPin, LOW); 
  delayMicroseconds(8400);
}